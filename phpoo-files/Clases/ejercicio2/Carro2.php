<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anio;
	private $circula;
	public $mensajeServidor;

	public function getCircula(){
		return $this->circula;
	}

	//declaracion del método verificación
	public function verificacion(){
		if($this->anio<1990){
			$this->circula="no circula";
		}elseif($this->anio>=1990 && $this->anio<=2010){
			$this->circula="se encuentra en revision";
		}else{
			$this->circula="si circula";
		}
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anio=$_POST['anio'];
	$Carro1->verificacion();
	$Carro1->mensajeServidor = "El carro ".$Carro1->getCircula(); ;

}




