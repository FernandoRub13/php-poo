<?php 
// crea una clase con  métodos (__construct y __destruct) pero e al momento de instanciar a la clase te genere una contraseña solo 4 letras mayúsculas y al momento de destruir el objeto se muestre en pantalla tu contraseña.

class Contraseña{
  //declaracion de propiedades
  public $contraseña;
  
  //constructor
  public function __construct(){
    $this->contraseña=strtoupper(substr(md5(uniqid(rand())),0,4));
  }

  //destructor
  public function __destruct(){
    echo "Tu contraseña es: ".$this->contraseña;
  }   

}